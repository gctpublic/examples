# Welcome to the documentation for the Gift Card Tank client template.

### Overview

The Gift Card Tank client template is HTML+JavaScript and is intended to be used as a starting point for customization.  The initial goal is to allow users to deploy a header+footer and provide some basic options for additional tweaking.  As a note, you most likely don't want to be making changes to the functionality of the form - outside of what exists in this document - but we do urge you to modify the appearance so that the page looks like your site.  

### Download

You can find multiple examples here: https://bitbucket.org/gctpublic/examples/src

* The basic example contains the default fields, and all you should need to do is update the gct_customer_id field with your username.
* The advanced example contains additional concepts that can be modified to fit your needs.

### SSL?

Yes, you should use SSL whenever possible.  However, the client doesn't POST back to your server, it sends the request directly to our API from the users browser.  Without SSL you aren't exposing any of the sensitive information that is being submitted.

### Modifications

You can modify different aspects of the client template for different outcomes.  The basic requirements that are marked with an asterisk should not be changed as they are required for the submission to be accepted correctly.

### [REQUIRED] Set the gct_customer_id to your username

```
<input type='hidden' id='gct_customer_id' value='USERNAME' />
```

### [OPTIONAL] You can also add a tracking value

```
<input type='hidden' id='gct_customer_track' value='TRACKING_CODE' />
```

### [OPTIONAL] Add additional_info fields

We allow you to add your own fields to the form, though we do not validate their contents.

```
<input type='text' id='employer' class='gct-additional-info validate' size='30' /> 
<input type='text' id='occupation' class='gct-additional-info validate' size='30' />
<input type='text' id='pet_gender' class='gct-additional-info validate' size='30' /> 
<input type='checkbox' id='affirmation' class='validate' title='This is required' />
```

The above is an example that will enforce multiple functionalities.

* The `gct-additional-info` class will be added and stored with the submission.
* The `validate` class forces the input to be required.
* Adding the title will force the error message if the user leaves the field blank.

### [OPTIONAL] Modify the thank you response.

You can modify the thank you response.
```
<div class='gct-resp-popup-message'>
<h2>Thank you for your donation!</h2>
<p>Thank you very much for your generous contribution.</p>
</div>
```
If the `gct-resp-popup-message` class is omitted, then a default will be used.